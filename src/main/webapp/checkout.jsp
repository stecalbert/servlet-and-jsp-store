<%--
  Created by IntelliJ IDEA.
  User: Albert.Stec
  Date: 03.01.2019
  Time: 23:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%--Skrypt blokujący przycisk wstecz przeglądarki--%>
    <script>
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            history.pushState(null, null, document.URL);
        });
    </script>
</head>
<body>

<h2>Total cash to pay:</h2>
<p>${totalPrice}</p>

<h2>Total cash spend by user:</h2>
<p>${totalCashSpend}</p>

<a href="/Products">Back to store</a>

</body>
</html>
