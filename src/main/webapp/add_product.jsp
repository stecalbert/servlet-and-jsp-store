<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<form action="doAdd" method="post">

    <label>Name</label>
    <input type="text" name="name" value="${storeProduct.getName()}" required/><br><br>

    <label>Description</label>
    <input type="text" name="description" value="${storeProduct.getDescription()}" required/><br><br>

    <label>Price</label>
    <input type="text" name="price" value="${storeProduct.getPrice()}" required/><br><br>

    <label>Quantity</label>
    <input type="number" name="quantity" value="${storeProduct.getQuantity()}" required/><br><br>

    <input type="submit" value="Update"/>
</form>
</body>
</html>
