package com.stecalbert.store.servlet;

import com.stecalbert.store.dao.CartDao;
import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.dao.factory.CartDaoFactory;
import com.stecalbert.store.dao.factory.ProductDaoFactory;
import com.stecalbert.store.model.Cart;
import com.stecalbert.store.model.StoreProduct;
import com.stecalbert.store.service.CartService;
import com.stecalbert.store.service.UserService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@WebServlet("/Cart")
public class CartServlet extends HttpServlet {
    private CartService cartService;

    @Override
    public void init() throws ServletException {
        CartDao cartDao = CartDaoFactory.getCartDao(Settings.DAO_TYPE);
        ProductDao productDao = ProductDaoFactory.getProductDao(Settings.DAO_TYPE);
        cartService = new CartService(cartDao, productDao);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = UserService.getUserCookie(req.getCookies()).get().getValue(); // zalogowany użytkownik musi posiadać ciasteczko
        Optional<Cart> optionalCart = cartService.getCartByUsername(username);
        if (optionalCart.isPresent() && !optionalCart.get().getProducts().isEmpty()) {
            Map<StoreProduct, Integer> products = cartService.getStoreProductsMatchingCartProducts(optionalCart.get());
            req.setAttribute("productsInCart", sortProductsById(products));
            req.getRequestDispatcher("cart.jsp").include(req, resp);
        } else {
            req.getRequestDispatcher("empty_cart.jsp").include(req, resp);
        }
    }

    private Map<StoreProduct, Integer> sortProductsById(Map<StoreProduct, Integer> unsortedProducts) {
        Comparator<Map.Entry<StoreProduct, Integer>> idComparator =
                Comparator.comparing(e -> e.getKey().getId());

        return unsortedProducts.entrySet().stream().
                sorted(idComparator).
                collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
