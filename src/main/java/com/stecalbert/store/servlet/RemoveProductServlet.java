package com.stecalbert.store.servlet;

import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.dao.factory.ProductDaoFactory;
import com.stecalbert.store.model.StoreProduct;
import com.stecalbert.store.service.ProductService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/removeStoreProduct")
public class RemoveProductServlet extends HttpServlet {
    private ProductService productService;

    @Override
    public void init() throws ServletException {
        ProductDao productDao = ProductDaoFactory.getProductDao(Settings.DAO_TYPE);
        productService = new ProductService(productDao);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.valueOf(req.getParameter("productId"));
        StoreProduct storeProduct = productService.getById(id);
        productService.delete(storeProduct);
        resp.sendRedirect("Products");
    }
}
