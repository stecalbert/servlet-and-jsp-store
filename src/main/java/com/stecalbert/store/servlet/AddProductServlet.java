package com.stecalbert.store.servlet;

import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.dao.factory.ProductDaoFactory;
import com.stecalbert.store.model.StoreProduct;
import com.stecalbert.store.service.ProductService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/showAddForm" /*pokaż formularz*/,
        "/doAdd" /* dodaj produkt z podanymi w formularzu danymi */})
public class AddProductServlet extends HttpServlet {
    private ProductService productService;

    @Override
    public void init() throws ServletException {
        ProductDao productDao = ProductDaoFactory.getProductDao(Settings.DAO_TYPE);
        productService = new ProductService(productDao);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case "/showAddForm":
                req.getRequestDispatcher("add_product.jsp").forward(req, resp);
                break;
            case "/doAdd":
                String name = req.getParameter("name");
                String description = req.getParameter("description");
                Integer price = Integer.valueOf(req.getParameter("price"));
                Integer quantity = Integer.valueOf(req.getParameter("quantity"));
                StoreProduct storeProduct = new StoreProduct(name, description, price, quantity);
                productService.insert(storeProduct);
                resp.sendRedirect("Products");
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
