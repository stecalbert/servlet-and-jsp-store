package com.stecalbert.store.servlet;

import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.dao.factory.UserDaoFactory;
import com.stecalbert.store.service.UserService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
    private UserService userService;

    @Override
    public void init() throws ServletException {
        UserDao userDao = UserDaoFactory.getUserDao(Settings.DAO_TYPE);
        userService = new UserService(userDao);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        boolean isValid = userService.validateUser(username, password);

        if (isValid) {
            // todo : dodać całego użytkownika jako atrybut
            req.getSession().setAttribute("username", username);
            Cookie cookie = userService.createCookie(username);
            resp.addCookie(cookie);
            resp.sendRedirect("Products");
        } else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
            resp.getWriter().println("<p>Invalid data or non existing user.</p>");
            requestDispatcher.include(req, resp);
        }
    }
}
