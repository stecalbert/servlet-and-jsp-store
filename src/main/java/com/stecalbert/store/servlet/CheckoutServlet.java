package com.stecalbert.store.servlet;

import com.stecalbert.store.dao.CartDao;
import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.dao.factory.CartDaoFactory;
import com.stecalbert.store.dao.factory.ProductDaoFactory;
import com.stecalbert.store.dao.factory.UserDaoFactory;
import com.stecalbert.store.model.Cart;
import com.stecalbert.store.model.User;
import com.stecalbert.store.service.CartService;
import com.stecalbert.store.service.UserService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

@WebServlet("/Checkout")
public class CheckoutServlet extends HttpServlet {
    private CartService cartService;
    private UserService userService;

    @Override
    public void init() throws ServletException {
        UserDao userDao = UserDaoFactory.getUserDao(Settings.DAO_TYPE);
        userService = new UserService(userDao);

        CartDao cartDao = CartDaoFactory.getCartDao(Settings.DAO_TYPE);
        ProductDao productDao = ProductDaoFactory.getProductDao(Settings.DAO_TYPE);
        cartService = new CartService(cartDao, productDao);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = UserService.getUserCookie(req.getCookies()).get().getValue(); // zalogowany użytkownik musi posiadać ciasteczko
        Optional<Cart> optionalCart = cartService.getCartByUsername(username);
        Cart cart = optionalCart.orElseThrow(() -> new RuntimeException("Undefined error occured")); // jeśli tu jesteśmy to karta musi istnieć, chyba że ktoś wchodzi po ścieżce

        // Zsumowanie całkowitej ceny wszystkich produktów
        BigDecimal totalPriceOfCartProducts = cartService.getTotalPriceOfCartProducts(cart);
        req.setAttribute("totalPrice", totalPriceOfCartProducts);

        // Dopisanie całkowitej ceny wszystkich produktów do całkowitej kwoty wydanej w tym sklepie przez użytkownika
        Optional<User> optionalUser = userService.getByUsername(username);
        User user = optionalUser.orElseThrow(() -> new RuntimeException("Undefined error occured"));
        BigDecimal totalCashSpend = user.getTotalCashSpend();
        BigDecimal totalCashSpendUpdated = totalCashSpend.add(totalPriceOfCartProducts);
        user.setTotalCashSpend(totalCashSpendUpdated);
        userService.update(user);
        req.setAttribute("totalCashSpend", totalCashSpendUpdated);

        // Wyczyszczenie karty, usunięcię produktów karty z bazy
        cart.getProducts().forEach(e -> cartService.deleteCartProduct(e, cart));

        req.getRequestDispatcher("checkout.jsp").forward(req, resp);
    }


}
