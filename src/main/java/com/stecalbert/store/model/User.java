package com.stecalbert.store.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@Entity(name = "users")
public class User {

    @SerializedName("id")
    @Expose
    @Id
    @GeneratedValue
    private Long id;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("totalCashSpend")
    @Expose
    @Column(name = "total_cash_spend")
    private BigDecimal totalCashSpend;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("surname")
    @Expose
    private String surname;

    @SerializedName("role")
    @Expose
    private String role;

    public User() {
    }

    public User(String username, String password, BigDecimal totalCashSpend, String name, String surname, String role) {
        this.username = username;
        this.password = password;
        this.totalCashSpend = totalCashSpend;
        this.name = name;
        this.surname = surname;
        this.role = role;
    }
}
