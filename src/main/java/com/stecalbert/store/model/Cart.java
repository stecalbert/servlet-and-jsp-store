package com.stecalbert.store.model;

import com.google.gson.annotations.Expose;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Entity(name = "carts")
public class Cart {

    @Id
    @GeneratedValue
    @Expose
    @Column(name = "cart_id")
    private Long id;

    @Column(name = "username")
    @Expose
    private String username;

    @OneToMany(mappedBy = "cart", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true, fetch = EAGER)
    @Expose
    private Set<ProductInCart> products;

    public Cart(String username) {
        this.username = username;
    }

    public Cart(String username, Set<ProductInCart> products) {
        this.username = username;
        this.products = products;
    }

    public Cart() {
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<ProductInCart> getProducts() {
        return products;
    }

    public void setProducts(Set<ProductInCart> products) {
        this.products = products;
    }


}
