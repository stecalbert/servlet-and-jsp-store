package com.stecalbert.store.filters;

import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.dao.factory.UserDaoFactory;
import com.stecalbert.store.model.User;
import com.stecalbert.store.service.UserService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebFilter(urlPatterns = {"/Products", "/products.jsp", "/AddProduct",
        "/index.jsp", "/", "/register.jsp", "/Register", "/Cart",
        "/doAdd", "/showEdit", "/doEdit", "/removeStoreProduct", "/add_product.jsp", "/edit_product", "/"})
public class AuthorizationFilter implements Filter {
    private UserService userService;

    @Override
    public void init(FilterConfig filterConfig) {
        UserDao userDao = UserDaoFactory.getUserDao(Settings.DAO_TYPE);
        userService = new UserService(userDao);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        Optional<Cookie> cookie = UserService.getUserCookie(req.getCookies());
        if (cookie.isPresent()) {
            String username = UserService.getUserCookie(req.getCookies()).get().getValue(); // zalogowany użytkownik musi posiadać ciasteczko
            Optional<User> optionalUserser = userService.getByUsername(username);
            User user = optionalUserser.orElseThrow(() -> new RuntimeException("User does not exist in db"));

            if (isLoginOrRegistration(req)) {

                // Jeśli użytkownik jest zalogowany i otwiera okno logowania lub rejestracji przekieruj go na listę produktów
                resp.sendRedirect("/Products");
            } else if (isAdminModule(req) && !user.getRole().equals("admin")) {

                // Jeśli użytkownik próbuje wejść na moduł admina, a nie ma uprawnień przekieruj go na listę produktów
                resp.sendRedirect("/Products");
            } else {

                // Jeśli użytkownik jest zalogowany i otwiera dowolne inne okno niż logowania wpuść go
                chain.doFilter(request, response);
            }
        } else {

            // Jeśli użytkownik nie jest zalogowany przekieruj go do okna logowania
            req.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    private boolean isAdminModule(HttpServletRequest req) {
        return req.getRequestURI().equals("/showAddForm")
                || req.getRequestURI().equals("/doAdd")
                || req.getRequestURI().equals("/showEdit")
                || req.getRequestURI().equals("/doEdit")
                || req.getRequestURI().equals("/removeStoreProduct")
                || req.getRequestURI().equals("/add_product.jsp")
                || req.getRequestURI().equals("/edit_product.jsp");
    }

    private boolean isLoginOrRegistration(HttpServletRequest req) {
        return req.getRequestURI().equals("/")
                || req.getRequestURI().equals("/index.jsp")
                || req.getRequestURI().equals("/register.jsp")
                || req.getRequestURI().equals("/Register");
    }
}
