package com.stecalbert.store.dao.factory;

import com.stecalbert.store.dao.CartDao;
import com.stecalbert.store.dao.db_impl.CartDaoDbImpl;
import com.stecalbert.store.dao.json_impl.CartDaoJsonImpl;
import com.stecalbert.store.settings.DaoType;

public class CartDaoFactory {

    public static CartDao getCartDao(DaoType daoType) {
        switch (daoType) {
            case JSON:
                return new CartDaoJsonImpl();
            case MY_SQL:
                return new CartDaoDbImpl();
            default:
                return new CartDaoJsonImpl();
        }
    }
}
