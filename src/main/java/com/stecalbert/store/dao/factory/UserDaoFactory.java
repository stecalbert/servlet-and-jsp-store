package com.stecalbert.store.dao.factory;

import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.dao.db_impl.UserDaoDbImpl;
import com.stecalbert.store.dao.json_impl.UserDaoJsonImpl;
import com.stecalbert.store.settings.DaoType;

public class UserDaoFactory {

    public static UserDao getUserDao(DaoType daoType) {
        switch (daoType) {
            case JSON:
                return new UserDaoJsonImpl();
            case MY_SQL:
                return new UserDaoDbImpl();
            default:
                return new UserDaoJsonImpl();
        }
    }
}
