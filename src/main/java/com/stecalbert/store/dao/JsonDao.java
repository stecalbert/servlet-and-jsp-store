package com.stecalbert.store.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stecalbert.store.model.JsonAccess;
import com.stecalbert.store.settings.Settings;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public interface JsonDao {
    Logger LOGGER = Logger.getLogger(JsonDao.class.getName());

    default JsonAccess getJson() {
        JsonAccess jsonAccess;
        try {
            InputStream is = new FileInputStream(Settings.JSON_PATH);
            InputStreamReader isr
                    = new InputStreamReader(is);
            BufferedReader reader
                    = new BufferedReader(isr);
            jsonAccess = new Gson().fromJson(reader, JsonAccess.class);
            is.close();
            LOGGER.log(Level.INFO, "Fetched json " + jsonAccess);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IOException", e);
            throw new RuntimeException("Cannot find file");
        }
        return jsonAccess;
    }

    default void writeJson(JsonAccess jsonAccess) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting().create();
        String json = gson.toJson(jsonAccess, JsonAccess.class);
        try {
            FileWriter fileWriter = new FileWriter(Settings.JSON_PATH);
            fileWriter.write(json);
            fileWriter.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IOException", e);
            throw new RuntimeException("Cannot find file");
        }
    }

}
