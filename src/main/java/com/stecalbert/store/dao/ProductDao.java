package com.stecalbert.store.dao;

import com.stecalbert.store.model.StoreProduct;

import java.util.List;

public interface ProductDao {
    List<StoreProduct> findAll();

    StoreProduct findById(Long id);

    boolean update(StoreProduct storeProduct);

    boolean delete(StoreProduct product);

    StoreProduct insert(StoreProduct user);
}
