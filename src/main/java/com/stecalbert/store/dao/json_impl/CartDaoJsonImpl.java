package com.stecalbert.store.dao.json_impl;

import com.stecalbert.store.dao.CartDao;
import com.stecalbert.store.dao.JsonDao;
import com.stecalbert.store.model.Cart;
import com.stecalbert.store.model.JsonAccess;
import com.stecalbert.store.model.ProductInCart;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CartDaoJsonImpl implements CartDao, JsonDao {
    private static final Logger LOGGER = Logger.getLogger(CartDaoJsonImpl.class.getName());

    @Override
    public Optional<Cart> findByUsername(String username) {
        List<Cart> carts = getJson().getShoppingCarts();
        Optional<Cart> optionalCart = carts.stream()
                .filter(e -> e.getUsername().equals(username))
                .findFirst();
        LOGGER.log(Level.INFO, "Fetched user from database: {0}" + optionalCart);
        return optionalCart;
    }

    @Override
    public boolean createOrUpdateCart(Cart cart) {
        JsonAccess jsonAccess = getJson();
        List<Cart> carts = jsonAccess.getShoppingCarts();
        carts.removeIf(e -> e.getUsername().equals(cart.getUsername()));
        carts.add(cart);
        jsonAccess.setShoppingCarts(carts);
        writeJson(jsonAccess);
        LOGGER.log(Level.INFO, "Updated cart : {0}" + cart);
        return true;
    }

    @Override
    public boolean deleteCartProduct(ProductInCart product, Cart cart) {
        cart.getProducts().removeIf(e -> e.getProductId().equals(product.getProductId()));
        JsonAccess jsonAccess = getJson();
        jsonAccess.getShoppingCarts().removeIf(e -> e.getUsername().equals(cart.getUsername()));
        jsonAccess.getShoppingCarts().add(cart);
        writeJson(jsonAccess);
        LOGGER.log(Level.INFO, "ProductInCart {0} deleted:", product);
        return true;
    }

}
