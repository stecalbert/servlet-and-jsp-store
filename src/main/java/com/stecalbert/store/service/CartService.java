package com.stecalbert.store.service;

import com.stecalbert.store.dao.CartDao;
import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.model.Cart;
import com.stecalbert.store.model.ProductInCart;
import com.stecalbert.store.model.StoreProduct;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

public class CartService {
    private CartDao cartDao;
    private ProductDao productDao;

    public CartService(CartDao cartDao, ProductDao productDao) {
        this.cartDao = cartDao;
        this.productDao = productDao;
    }

    public Optional<Cart> getCartByUsername(String username) {
        return cartDao.findByUsername(username);
    }

    public boolean addToCart(Long productId, String username) {
        Optional<Cart> optionalCart = cartDao.findByUsername(username);
        Cart cart = optionalCart.orElseGet(() -> new Cart(username, new HashSet<>()));
        createCartProductOrIncrementQuantityAndGet(cart, productId);
        cartDao.createOrUpdateCart(cart);
        decrementProductQuantityInStore(productId);
        return true;
    }

    public boolean removeFromCart(Long productId, String username) {
        Cart cart = cartDao.findByUsername(username).get();
        removeCartProductOrDecrementQuantityAndGet(cart, productId);
        incrementProductQuantityInStore(productId);
        cartDao.createOrUpdateCart(cart);
        return true;
    }

    /**
     * Jeśli produkt istnieje w karcie, to usuń go z karty i zinkrementuj ilość o 1.
     * W przeciwnym wypadku stwórz nowy produkt o qunatity = 1
     *
     * @param cart      - Karta obecnego użytkownika
     * @param productId - id produktu do doania
     * @return - produkt do dodania
     */
    private void createCartProductOrIncrementQuantityAndGet(Cart cart, Long productId) {
        Optional<ProductInCart> optionalProduct = filterCartProductsById(cart, productId);
        ProductInCart product;
        if (optionalProduct.isPresent()) {
            product = optionalProduct.get();
            cart.getProducts().remove(product);
            product.incrementQuantity();
        } else {
            product = new ProductInCart(productId, 1, cart);
        }
        cart.getProducts().add(product);
    }

    /**
     * Jeśli produkt istnieje w karcie i jest go więcej niż 1 sztuka to zmiejsz ilość o 1
     * W przeciwnym wypadku usuń całkowicie z karty
     *
     * @param cart      - Karta obecnego użytkownika
     * @param productId - id produktu do doania
     * @return - produkt do dodania
     */
    private void removeCartProductOrDecrementQuantityAndGet(Cart cart, Long productId) {
        ProductInCart cartProduct = filterCartProductsById(cart, productId).get();
        cart.getProducts().remove(cartProduct);
        if (cartProduct.getQuantity() > 1) {
            cartProduct.decrementQuantity();
            cart.getProducts().add(cartProduct);
        }
    }

    private Optional<ProductInCart> filterCartProductsById(Cart cart, Long productId) {
        return cart
                .getProducts()
                .stream()
                .filter(e -> e.getProductId().equals(productId))
                .findFirst();
    }

    /**
     * Pobiera mape z produktami (StoreProduct) na podstawie produktów w karcie (ProductInCart)
     *
     * @param cart - Karta obecnego użytkownika
     * @return Map - mapa, w której wartość to ilość produktu w karcie, a klucz to StoreProduct
     */
    public Map<StoreProduct, Integer> getStoreProductsMatchingCartProducts(Cart cart) {
        Map<StoreProduct, Integer> products = new HashMap<>();
        cart.getProducts().forEach(e ->
                {
                    StoreProduct storeProduct = productDao.findById(e.getProductId());
                    products.put(storeProduct, e.getQuantity());
                }
        );
        return products;
    }

    private void decrementProductQuantityInStore(Long productId) {
        StoreProduct storeProduct = productDao.findById(productId);
        storeProduct.decrementQuantity();
        productDao.update(storeProduct);
    }

    private void incrementProductQuantityInStore(Long productId) {
        StoreProduct storeProduct = productDao.findById(productId);
        storeProduct.incrementQuantity();
        productDao.update(storeProduct);
    }

    public BigDecimal getTotalPriceOfCartProducts(Cart cart) {
        BigDecimal totalPrice = new BigDecimal(0.0);
        for (ProductInCart productInCart : cart.getProducts()) {
            StoreProduct storeProduct = productDao.findById(productInCart.getProductId());
            BigDecimal price = new BigDecimal(storeProduct.getPrice());
            BigDecimal quantity = BigDecimal.valueOf(productInCart.getQuantity());
            totalPrice = totalPrice.add(price.multiply(quantity));
        }
        return totalPrice;
    }

    public boolean deleteCartProduct(ProductInCart productInCart, Cart cart) {
        return cartDao.deleteCartProduct(productInCart, cart);
    }
}
